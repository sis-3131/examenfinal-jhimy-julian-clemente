import { instanseCat } from "../configuration/config"

export const getCat = async () =>{
   const response = await instanseCat.get ('/images/search?limit=1')    
   return response.data
}
