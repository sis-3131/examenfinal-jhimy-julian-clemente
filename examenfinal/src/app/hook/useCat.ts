
import { useEffect, useState } from "react"
import { getCat } from "../services/cat.services"

export const useCat =  () => {
    const [cat,setCat] = useState <any> ()
    const [loading,setLoading] = useState<boolean>(true)

    const fetchData = async () => {
        try{
            const responseCat = await getCat()
            setCat(responseCat)
            setLoading(false)
        }catch(e){
            console.log(e) 
            setLoading(false)
        }
       
    }
    useEffect(() =>{
        fetchData()
    },[])

    return {cat,loading}
}
