"use client"
import { Button, Card, Grid, TextField } from "@mui/material";
import AppBar from "./componentes/AppBar";
import CardBitcoin from "./componentes/CardBitcoin";
import CardServices from "./componentes/CardServices";
import { useCat } from "./hook/useCat";
import CardCat from "./componentes/CardCat";
import Image from "next/image";
import CardPerfil from "./componentes/CardPerfil";
import Footer from "./componentes/Footer";


export default function Home() {

  return (
    <main>
     <AppBar></AppBar>
    <div>
    <div className="fondo"></div>
    <div className="title">
    <h1>Start and Build Your Crypto Portfolio Here</h1>
    <p>Only at CryptoCap, you can build a good portfolio and learn</p>
    <p className="p">best practices about cryptocurrency.</p>
    <Button variant="contained" className="boton1">Get Starded</Button>
    </div>
    <div>
    <h1 className="title4"> Market Trend</h1>
    <CardBitcoin></CardBitcoin>
    </div>
    </div>
    <div className="title5">
      <h1>CryptoCap Amazing Faetures</h1>
      <p>Explore sensational features to prepare your best investment in cryptocurrency</p>
      <CardServices></CardServices>
    </div>
    <div className="title6">
    <h1>New In Cryptocurrency?</h1>
    <p>We'll tell you what cryptocurrencies are, how they work and why you should own one right now. So let's do it.</p>
    <Button variant="contained">Contained</Button>
    </div>
    <div className="title7">
      <h1>Market Update</h1>
      <p>Cryptocurrency Categories</p>
      <div className="botones1">
      <Button variant="text" sx={{backgroundColor:"white", color:"black"}}>Popular</Button>
      <Button variant="text">Metaverse</Button>
      <Button variant="text">Metaverse</Button>
      <Button variant="text">Energy</Button>
      <Button variant="text">Gaming</Button>
      <Button variant="text">Music</Button>
      <Button variant="text">See All 12+</Button>
      </div>
      <TextField id="outlined-basic" label="Search Coin" variant="outlined" className="seachr2"/>
    </div>
    <div className="title8">
    <div>
      <h2>NO</h2>
      <hr className="hr"></hr>
      <p className="n">1</p>
      <hr className="hr"></hr>
      <p className="n">2</p>
      <hr className="hr"></hr>
      <p className="n">3</p>
      <hr className="hr"></hr>
      <p className="n">4</p>
      <hr className="hr"></hr>
      <p className="n">5</p>
      <hr className="hr"></hr>
      <p className="n">6</p>
      <hr className="hr"></hr>
      <p className="n">7</p>
      
    </div>
    <div className="logos">
      <h2>NAME</h2>
    </div>
    </div>
    <div className="title9">
    <div>
      <h2>LAST PRICE</h2>
      <p><br />$56,623.54</p>
      <p><br />$56,623.54</p>
      <p><br />$56,623.54</p>
      <p><br />$56,623.54</p>
      <p><br />$56,623.54</p>
      <p><br />$56,623.54</p>
      <p><br />$56,623.54</p>
    </div>
    <div>
      <h2>CHANGE</h2>
      <p><br />1.41%</p>
      <p><br />2.22%</p>
      <p><br />-0.82%</p>
      <p><br />-0.03%</p>
      <p className="p2"><br />-0.53%</p>
      <p className="p2"><br />-0.53%</p>
      <p className="p2"><br />-0.53%</p>
    </div>
    <div>
      <h2>MARKET STATS</h2>
    </div>
    <div>
      <h2>TRADE</h2>
      <Button variant="contained">Contained</Button>
      <br /><br />
      <Button variant="contained">Contained</Button>
      <br />
      <br />
      <Button variant="contained">Contained</Button>
      <br />
      <br />
      <Button variant="contained">Contained</Button>
      <br />
      <br />
      <Button variant="contained">Contained</Button>
      <br />
      <br />
      <Button variant="contained">Contained</Button>
      <br />
      <br />
      <Button variant="contained">Contained</Button>
    </div>
    </div>
    <div>
      <div className="link">
      <Button>See All Coins</Button>
      </div>
    </div>
    <div className="title10">
      <h1>How To Get Started</h1>
      <p>Simple and easy way to start your investment 
      in cryptocurrency</p>
      <Button variant="contained">Get Started</Button>
    </div>
    <div>
      <CardPerfil></CardPerfil>
    </div>
    <div className="fondo2"></div>
    <div className="title11">
      <h1>Learn About Cryptocurrency</h1>
      <p>Learn all about cryptocurrency to start investing</p>

      <Button variant="contained" className="botonfinal">See All Articles</Button>
    </div>
    <div>

    </div>
    <div className="titlefinal">
      <h1>Crypto </h1><h1 className="subtitle">Cap</h1>
      <div className="redes">
      <Image src={"/images/insta.svg"} alt={""} width={50} height={30}></Image>
      <Image src={"/images/face.svg"} alt={""} width={50} height={30}></Image>
      <Image src={"/images/x.svg"} alt={""} width={50} height={30}></Image>
      <Image src={"/images/youtube.svg"} alt={""} width={50} height={30}></Image>
      <p className="p3">2021 CoinMarketCap. All rights reserved</p>
      </div>
    </div>
    <div className="footer">
      <Footer title={"About Us"} subtitle={"About"} subtitle2={"Careers"} subtitle3={"Blog"} subtitle4={"Legal & privacy"}></Footer>
      <Footer title={"Services"} subtitle={"Affilliate"} subtitle2={"Buy Crypto"} subtitle3={"Affilliate"} subtitle4={"Institutional Services"}></Footer>
      <Footer title={"Learn"} subtitle={"What is Cryptocurency?"} subtitle2={"Crypto Basic"} subtitle3={"Tips and Tutorials"} subtitle4={"Market Update"}></Footer>
    </div>
    </main>
  );
}
