import axios from "axios";

export const instanseCat = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_CAT,
    timeout: 5000,
    headers: {'X-Custom-Header': 'foobar'}
});

instanseCat.interceptors.request.use(function(config){
    return config;
}, function (error){
    return Promise.reject(error);
})
