import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import Image from 'next/image';
import { Button } from '@mui/material';

interface ExpandMoreProps extends IconButtonProps {
  expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function RecipeReviewCard() {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <div className='card1'>
      <><><><Card sx={{ maxWidth: 345, height: "200px" }} className='card'>
      <CardHeader
        avatar={<Avatar sx={{}} aria-label="recipe">
          <Image src={'/images/logo1.svg'} alt={''} width={50} height={50}></Image>
        </Avatar>}
        title="BTC"
        subheader="" />
      <Button className='boton2'><Image src={'/images/flecha.svg'} alt={''} width={50} height={50}></Image></Button>
      <Button variant="contained" className='BOTON3'>BITCOIN</Button>
      <div className='title2'>
        <h4>$56,623.54</h4>
        <p>1.41%</p>
        <Image src={'/images/grafic.svg'} alt={''} width={105} height={125} className='grafic'></Image>
      </div>
      <CardMedia />
    </Card>

      <Card sx={{ maxWidth: 345, height: "200px" }} className='card'>
        <CardHeader
          avatar={<Avatar sx={{}} aria-label="recipe">
            <Image src={'/images/logo2.svg'} alt={''} width={50} height={50}></Image>
          </Avatar>}
          title="BTC"
          subheader="" />
        <Button className='boton2'><Image src={'/images/flecha.svg'} alt={''} width={50} height={50}></Image></Button>
        <Button variant="contained" className='BOTON3'>ETHEREUM</Button>
        <div className='title2'>
          <h4>$4,267.90</h4>
          <p>2.22%</p>
          <Image src={'/images/grafic.svg'} alt={''} width={105} height={125} className='grafic'></Image>
        </div>
        <CardMedia />
      </Card></>

      <Card sx={{ maxWidth: 345, height: "200px" }} className='card'>
        <CardHeader
          avatar={<Avatar sx={{}} aria-label="recipe">
            <Image src={'/images/logo3.svg'} alt={''} width={50} height={50}></Image>
          </Avatar>}
          title="BTC"
          subheader="" />
        <Button className='boton2'><Image src={'/images/flecha.svg'} alt={''} width={50} height={50}></Image></Button>
        <Button variant="contained" className='BOTON3'>BINANCE</Button>
        <div className='title2'>
          <h4>$587.74</h4>
          <p>0.82%</p>
          <Image src={'/images/grafic.svg'} alt={''} width={105} height={125} className='grafic'></Image>
        </div>
        <CardMedia />
      </Card></>
      
      <Card sx={{ maxWidth: 345, height: "200px" }} className='card'>
        <CardHeader
          avatar={<Avatar sx={{}} aria-label="recipe">
            <Image src={'/images/logo4.svg'} alt={''} width={50} height={50}></Image>
          </Avatar>}
          title="BTC"
          subheader="" />
        <Button className='boton2'><Image src={'/images/flecha.svg'} alt={''} width={50} height={50}></Image></Button>
        <Button variant="contained" className='BOTON3'>TETHER</Button>
        <div className='title2'>
          <h4>$0.9998</h4>
          <p>0,03%</p>
          <Image src={'/images/grafic.svg'} alt={''} width={105} height={125} className='grafic'></Image>
        </div>
        <CardMedia />
      </Card></>
    
    </div>
  );
}