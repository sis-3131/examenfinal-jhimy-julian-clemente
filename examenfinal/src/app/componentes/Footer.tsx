export default function Footer(props:{title:string, subtitle:string,subtitle2: string,subtitle3: string, subtitle4: string}){
    return(
        <div>
            <h1>{props.title}</h1>
            <p>{props.subtitle}</p>
            <p>{props.subtitle2}</p>
            <p>{props.subtitle3}</p>
            <p>{props.subtitle4}</p>
        </div>
    )
}