import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import Image from 'next/image';
import { Button } from '@mui/material';

interface ExpandMoreProps extends IconButtonProps {
  expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function RecipeReviewCard() {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
   <div className='cardp'>
     <><><><Card sx={{ maxWidth: 345 }} className='card2'>
          <CardHeader
              avatar={<Avatar aria-label="recipe" sx={{ width: "100px", height: "100px" }}>
                  <Image src={'/images/logo5.svg'} alt={''} width={80} height={80}></Image>
              </Avatar>} />
          <CardContent>
              <h3>Manage Portfolio</h3>
              <p className='P2'>Buy and sell popular digital currencies, keep track of them in the one place.</p>
              <Button variant="text" sx={{ color: "#0FAE96" }}>See Explained <Image src={'/images/flecha2.svg'} alt={''} width={50} height={30}></Image></Button>
          </CardContent>
      </Card>

          <Card sx={{ maxWidth: 345 }} className='card2'>
              <CardHeader
                  avatar={<Avatar aria-label="recipe" sx={{ width: "100px", height: "100px" }}>
                      <Image src={'/images/logo6.svg'} alt={''} width={80} height={80}></Image>
                  </Avatar>} />
              <CardContent>
                  <h3>Protected Securely</h3>
                  <p className='P2'>All cash balances are covered by FDIC insurance, up to a maximum of $250,000.</p>
                  <Button variant="text" sx={{ color: "#0FAE96" }}>See Explained <Image src={'/images/flecha2.svg'} alt={''} width={50} height={30}></Image></Button>
              </CardContent>
          </Card></>

          <Card sx={{ maxWidth: 345 }} className='card2'>
              <CardHeader
                  avatar={<Avatar aria-label="recipe" sx={{ width: "100px", height: "100px" }}>
                      <Image src={'/images/logo7.svg'} alt={''} width={80} height={80}></Image>
                  </Avatar>} />
              <CardContent>
                  <h3>Cryptocurrency Variety</h3>
                  <p className='P2'>Supports a variety of the most popular digital currencies and always uptodate.</p>
                  <Button variant="text" sx={{ color: "#0FAE96" }}>See Explained <Image src={'/images/flecha2.svg'} alt={''} width={50} height={30}></Image></Button>
              </CardContent>
          </Card></>
          
          <Card sx={{ maxWidth: 345 }} className='card2'>
              <CardHeader
                  avatar={<Avatar aria-label="recipe" sx={{ width: "100px", height: "100px" }}>
                      <Image src={'/images/logo8.svg'} alt={''} width={80} height={80}></Image>
                  </Avatar>} />
              <CardContent>
                  <h3>Learn Best Practice</h3>
                  <p className='P2'>Easy to know how to cryptocurrency works and friendly to newbie.</p>
                  <Button variant="text" sx={{ color: "#0FAE96" }}>See Explained <Image src={'/images/flecha2.svg'} alt={''} width={50} height={30}></Image></Button>
              </CardContent>
          </Card></>
   </div>
  );
}