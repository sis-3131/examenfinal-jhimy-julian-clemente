import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Avatar from '@mui/material/Avatar';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';;
import { Box } from '@mui/material';

interface CardServiceProps {
   
    title: string;
    image: string;
    name: string;
    date: string
}

export default function CardCat({  title, image, name, date }: CardServiceProps) {
    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    return (
        <Card sx={{ maxWidth: 300, paddingX: "20px", paddingY: "20px", borderRadius: "20px", boxShadow: "5px 10px #888888", height: "100%" }} className='apis'>
            <CardMedia
                component="img"
                height="194"
                image={image}
                alt="Paella dish"
            />
            <CardContent sx={{height:"150px",display:"flex",flexDirection:"column",justifyContent:"space-between"}}>
                <Typography variant="h6">
                    {title}
                </Typography>
                <Box sx={{ display: "flex",width:"100%", alignItems: "end"}}>
                    <Box sx={{display: "flex"}}>
                      
                        <Box>
                            <Typography variant="body2" color="text.secondary">
                                {name}
                            </Typography>
                            <Typography variant="body2" color="text.secondary" className="truncate">
                                {date}
                            </Typography>
                        </Box>
                    </Box>
                </Box>
            </CardContent>
        </Card>
    );
}
